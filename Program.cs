﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace _24paralllelLINQHomework
{
    class Program
    {
        const int Count = 10_000_000;
        private static int[] Array = new int[Count];

        static void Main(string[] args)
        {
            TimeTest(ForTest);
            Console.WriteLine();
            TimeTest(ParallelForTest);
            Console.WriteLine();
            TimeTest(ParallelLinqTest);
            Console.ReadLine();
        }

        static void GenerateArray()
        {
            for (int i = 0; i < Array.Length - 1; i++)
            {
                Array[i] = i;
            }
        }

        public static void TimeTest(Action act)
        {
            GenerateArray();
            long[] timerValueArray = new long[50];
            for (int i = 0; i < 5; i++)
            {
                var timer = Stopwatch.StartNew();
                act();
                timer.Stop();
                timerValueArray[i] = timer.ElapsedMilliseconds;
            }
            Console.WriteLine($"Время: {timerValueArray.Average()} мс.");
        }

        public static void ForTest()
        {
            TimeTest(() =>
            {
                int sum = 0;
                var n = Array.Length;
                for (var i = 0; i < n; i++)
                {
                    sum += Array[i];
                }
            });
        }

        public static void ParallelForTest()
        {
            int sum = 0;
            TimeTest(() =>
            {
                ParallelLoopResult result = Parallel.ForEach(Array, (item) =>
                {
                    sum += item;
                });
            });
        }

        public static void ParallelLinqTest()
        {
            int sum = 0;
            TimeTest(() =>
            {
                Array.AsParallel().ForAll(x => sum += Array[x]);
            });
        }

        static void Show(int[] Array)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < 10; i++)
            {
                sb.Append(Array[i].ToString() + " ");
            }
            Console.WriteLine(sb);
        }



    }
}
